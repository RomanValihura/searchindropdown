chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
    if (changeInfo.status == 'complete' && tab.active) {
        chrome.tabs.executeScript(
            tabId,
            {
                file: 'bundle.js',
                allFrames: true
            },
            () => {
                chrome.tabs.insertCSS(tabId, {file: "style.css"});
            }
        );

    }
});