var React = require('react');
var ReactDOM = require('react-dom');
var ReactDropdown = require('./ReactDropdown');

var $ = document.jQuery || require("jquery");
var selects = $('select');

function forEach(collection, callback) {
    Array.prototype.forEach.call(collection, callback);
}

function map(collection, callback) {
    Array.prototype.map.call(collection, callback);
}

forEach(selects, replaceToInput);

function replaceToInput(select) {
    var renderTarget = document.createElement('div');
    select.parentNode.appendChild(renderTarget);
    select.style.display = 'none';

    ReactDOM.render(
        <ReactDropdown
            countVisibleItems={10}
            dropDownItems={getLiFromSelect(select)}
            inputClass={select.className}
            container={{
                top: select.offsetTop,
                left: select.offsetLeft
            }}
        />,
        renderTarget
    );
}

function getLiFromSelect(select) {
    var liContainer = [];

    forEach(select.querySelectorAll('option'), (option) => {
        liContainer.push({
            id: option.value,
            value: option.innerText
        });
    });

    return liContainer;
}