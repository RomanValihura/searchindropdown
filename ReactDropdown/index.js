var React = require('react');
var DropdownItem = require('./Item');
var $ = require('jquery');

const LIST_CLASS  = 'plugin-select-element-container';

module.exports = React.createClass({
    hideWhenBlur: true,

    propTypes: {
        countVisibleItems : React.PropTypes.number,
        dropDownItems     : React.PropTypes.array.isRequired,
        inputClass        : React.PropTypes.string.isRequired
    },

    getInitialState: function () {
        var items = this.props.dropDownItems;

        return {
            inputValue        : '',
            dropDownItems     : items,
            selectedItem      : items.length > 0 ? items[0].value : '',
            displayList       : false,
            topScrollPosition : 0
        };
    },

    componentDidUpdate: function () {
        var input = $(this.refs.searchDropdown),
            ul = $(this.refs.searchItems),
            inputHeight = input.outerHeight(),
            inputOffset = input.offset(),
            inputWidth  = input.outerWidth();


        ul.offset({
            top: inputOffset.top + inputHeight,
            left: inputOffset.left
        });

        ul.width(inputWidth);
    },

    onInputChange: function (e) {
        var filtered = this.filterMenu(e.target.value);
        this.setState({
            inputValue: e.target.value,
            dropDownItems: filtered,
            selectedItem: filtered.length > 0 ? filtered[0].value : ''
        });
    },

    onKeyDown: function (e) {
        switch (e.key) {
            case 'ArrowDown':
                e.preventDefault();
                this.itemDown();
                break;

            case 'ArrowUp':
                e.preventDefault();
                this.itemUp();
                break;

            case 'Enter':
                e.preventDefault();
                if(this.state.displayList) {
                    this.selectElement(this.state.selectedItem);
                    this.hideList();
                    return;
                }
                break;
        }

        this.showList();
    },

    showList: function () {
        this.setState({
            displayList: true
        });
    },

    hideList: function () {
        this.setState({
            displayList: false
        });
    },

    selectElement: function(elementValue) {
        this.setState({
            selectedItem: elementValue,
            inputValue: elementValue
        });
    },

    filterMenu: function (string) {
        var allItems = this.props.dropDownItems;
        return string
            ? allItems.filter(
                item => item.value.search(
                    new RegExp(string.replace(/[.?*+^$[\]\\(){}|-]/g, "\\$&"), "i")
                ) !== -1
            )
            : this.props.dropDownItems;
    },

    itemDown: function () {
        var currentIndex = this.getSelectedElementIndex();

        if(currentIndex == this.state.dropDownItems.length - 1) {
            return;
        }

        this.selectElement(
            this.state.dropDownItems[currentIndex + 1].value
        )
    },

    itemUp: function () {
        var currentIndex = this.getSelectedElementIndex();

        if(currentIndex == 0) {
            return;
        }

        this.selectElement(
            this.state.dropDownItems[currentIndex - 1].value
        )
    },

    scrollToCurrentTop: function() {

    },

    scrollToCurrentBottom: function() {

    },

    getSelectedElementIndex: function () {
        return this.state.dropDownItems.findIndex(element => element.value === this.state.selectedItem)
    },

    render: function () {
        return (
            <div ref="replacedContainer">
                <input
                    className={this.props.inputClass}
                    onChange={this.onInputChange}
                    onKeyDown={this.onKeyDown}
                    onFocus={this.showList}
                    onBlur={() => {this.hideWhenBlur ? this.hideList() : false}}
                    type="text"
                    ref="searchDropdown"
                    value={this.state.inputValue}
                />

                <ul
                    className={LIST_CLASS}
                    style={{
                        visibility: this.state.displayList ? 'visible' : 'hidden'
                    }}
                    onMouseOver={() => this.hideWhenBlur = false}
                    onMouseOut={() => this.hideWhenBlur = true}
                    ref="searchItems"
                >
                    {this.state.dropDownItems.length > 0 ?
                        this.state.dropDownItems.map((item, key) => {
                            return (
                                <DropdownItem
                                    onClick={element => {
                                        this.selectElement(element);
                                        this.hideList();
                                    }}
                                    key={key}
                                    id={item.id}
                                    value={item.value}
                                    selected={item.value == this.state.selectedItem}
                                />
                            )
                        }) :
                        <DropdownItem value="Not found"/>
                    }
                </ul>
            </div>
        );
    }
});