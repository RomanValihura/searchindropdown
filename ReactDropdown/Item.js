var React = require('react');

const ITEM_CLASS = 'plugin-select-element-item';
const ITEM_SELECTED_CLASS = 'plugin-select-element-item-selected';

module.exports = React.createClass({
    propTypes: {
        id: React.PropTypes.string,
        value: React.PropTypes.string
    },

    getDefaultProps: function() {
        return {
            selected: false
        };
    },

    getClass: function () {
        return this.props.selected ? `${ITEM_CLASS} ${ITEM_SELECTED_CLASS}` : ITEM_CLASS
    },

    onClick: function () {
        this.props.onClick(this.props.value);
    },

    render: function () {
        return (
            <li onClick={this.onClick} className={this.getClass()} >
                {this.props.value}
            </li>
        )
    }
});